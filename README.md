# demo-k8s-shared-runner

This project demonstrates how to use the kubernetes+executor shared runners for CI jobs.

See [Kubernetes+executor Shared Runners](https://gitlab.docs.cern.ch/docs/Build%20your%20application/CI-CD/Runners/k8s-shared-runners) for details.
